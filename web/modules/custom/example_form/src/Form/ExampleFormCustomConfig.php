<?php

namespace Drupal\example_form\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ExampleFormCustomConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'example_form.example_form_custom_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['example_form.custom_config'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->config('example_form.custom_config')->get('title'),
    ];

    $form['body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $this->config('example_form.custom_config')->get('body'),
    ];

    $form['#attached']['library'][] = 'example_form/example_form.validate_form';

    return parent::buildForm($form, $form_state);
  }

//  /**
//   * {@inheritdoc}
//   */
//  public function validateForm(array &$form, FormStateInterface $form_state) {
//    $title = $form_state->getValue('title');
//    if (strlen($title) < 5) {
//      $form_state->setErrorByName('title', $this->t('The title must be at least 5 characters long.'));
//    }
//
//    parent::validateForm($form, $form_state);
//  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('example_form.custom_config')
      ->set('title', $form_state->getValue('title'))
      ->set('body', $form_state->getValue('body'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
