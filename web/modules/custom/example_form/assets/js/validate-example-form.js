/**
 * @file
 * JavaScript for registration_form.
 */

let submit = document.getElementById('edit-submit')

let title = document.getElementById('edit-title');

function validateTitle(){
  if (title.value.length < 5){
    title.classList.add('error')
    submit.classList.add('disabled')
  }
  else {
    title.classList.remove('error')
    submit.classList.remove('disabled')
  }
}

title.addEventListener('input', validateTitle)
