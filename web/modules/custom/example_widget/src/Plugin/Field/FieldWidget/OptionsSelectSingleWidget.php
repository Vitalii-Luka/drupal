<?php

namespace Drupal\example_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'options_select_single' widget.
 *
 * @FieldWidget(
 *   id = "options_select_single",
 *   label = @Translation("Select list (single)"),
 *   field_types = {
 *     "entity_reference",
 *     "list_integer",
 *     "list_float",
 *     "list_string"
 *   }
 * )
 */
class OptionsSelectSingleWidget extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta];

    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['#default_value'] = $item->getValue();
    $element['#multiple'] = FALSE;

    return ['value' => $element];
  }


    /**
     * {@inheritdoc}
     */
    public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
      foreach ($values as $key => $value) {
        // The entity_autocomplete form element returns an array when an entity
        // was "autocreated", so we need to move it up a level.
        if (isset($value['value'][0]) && is_array($value['value'][0])) {
          unset($values[$key]['value']);
          $values[$key] += $value['value'][0];
        }
        elseif(isset($values[$key])) {
          unset($values[$key]);
        }
      }

      return $values;
    }

}
