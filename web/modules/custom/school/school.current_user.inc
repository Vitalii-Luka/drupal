<?php

/**
 * @file
 */

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_current_user(&$variables) {
  $data = \Drupal::service('_school.custom_services')->getData();
  $variables['login_user'] = $data;
}
