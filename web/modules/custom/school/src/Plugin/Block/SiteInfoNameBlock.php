<?php

namespace Drupal\school\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Show Site Name' block.
 *
 * @Block(
 *   id = "site_info_name_block",
 *   admin_label = @Translation("Site info name block"),
 *   category = @Translation("Custom block"),
 * )
 */
class SiteInfoNameBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
      '#theme' => 'site_info_name',
    ];
  }

}
