<?php

namespace Drupal\school\Services;

use Drupal\Core\Session\AccountInterface;

/**
 * Class CustomService
 *
 * @package Drupal\mymodule\Services
 */
class CustomService {

  protected AccountInterface $currentUser;

  /**
   * CustomService constructor.
   * @param AccountInterface $currentUser
   */
  public function __construct(AccountInterface $currentUser) {
    $this->currentUser = $currentUser;
  }


  /**
   * @return \Drupal\Component\Render\MarkupInterface|string
   */
  public function getData() {
    return $this->currentUser->getAccountName();
  }

}
