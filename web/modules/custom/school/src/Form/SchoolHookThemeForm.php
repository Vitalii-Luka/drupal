<?php

namespace Drupal\school\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class HookThemeFormExample
 *
 * @package Drupal\school\Form
 */
class SchoolHookThemeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    return 'hook_theme_form_example';

  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['name'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => 'Full name',
      ],
    ];

    $form['email'] = [
      '#type' => 'email',
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => 'Email*',
      ],
    ];

    $form['country'] = [
      '#type' => 'email',
      '#attributes' => [
        'placeholder' => 'Your Country',
      ],
    ];

    $form['phone'] = [
      '#type' => 'tel',
      '#attributes' => [
        'placeholder' => 'Phone number',
      ],
    ];

    $form['comment'] = [
      '#type' => 'textarea',
      '#attributes' => [
        'placeholder' => 'Leave us a comment',
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    drupal_set_message('Good Job!');
  }

}
