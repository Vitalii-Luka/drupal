<?php

namespace Drupal\school\Access;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;

class CustomAccessCheck implements AccessInterface {

  /**
   * A custom access check.
   *
   * @return AccessResultInterface
   *   The access result.
   */
  public function access(): AccessResultInterface {
    $data = date("d");
    if ($data % 2 == 0) {
      return AccessResult::allowed();
    } else {
      return AccessResult::forbidden();
    }
  }
}
