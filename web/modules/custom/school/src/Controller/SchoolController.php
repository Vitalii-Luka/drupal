<?php

namespace Drupal\school\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements methods for base routes.
 */
class SchoolController extends ControllerBase implements ContainerInjectionInterface {

  protected $loggerFactory;

  /**
   * SchoolController constructor.
   * @param LoggerChannelFactoryInterface $logger_factory
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory)
  {
    $this->loggerFactory = $logger_factory->get('drupal_school');
  }

  /**
   * @param ContainerInterface $container
   *
   * @return ContainerInjectionInterface|static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')
    );
  }

  public function customPage() {

    $message = "Drupal School";
    $this->loggerFactory->info($message);
    $this->loggerFactory->error($message);
    $this->loggerFactory->alert($message);
    $this->loggerFactory->debug($message);
    $this->loggerFactory->critical($message);
    $this->loggerFactory->emergency($message);
    $this->loggerFactory->notice($message);
    $this->loggerFactory->warning($message);
    $this->loggerFactory->log('info', $message);

    return [
      '#theme' => 'current_user',
    ];
  }

}
