<?php

namespace Drupal\school\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Implements methods for showing the current date.
 */
class SchoolCurrentDateController extends ControllerBase {

  public function CurrentDate() {
    return [
      '#theme' => 'current_date',
    ];
  }

}
