

## Drupal 9 ##

Local development infrastructure consists of:

```
- PHP 7.4
- Nginx
- MySQL
- phpMyAdmin
- Mailhog
```

[Drush commands](https://drushcommands.com)

[Lando commands](https://docs.lando.dev/basics/usage.html#default-commands)

[Docker commands](https://docs.docker.com/engine/reference/commandline/docker/)
